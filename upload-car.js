import { statSync } from 'fs'
import { CarIndexedReader } from '@ipld/car/indexed-reader'
import { makeStorageClient } from './lib.js'
import yargs from 'yargs'

async function storeCARs (cars) {
   const client = makeStorageClient()
   const cids= []
   const totalSize = cars.map(f => statSync(f).size).reduce((a, b) => a + b, 0)
   let uploaded = 0

   function onStoredChunk(size) {
      function padWithZero(num, targetLength) {
         return String(num).padStart(targetLength, '0');
      }
      uploaded += size
      const pct = 100 * ( uploaded / totalSize )
      console.log(`Uploading... ${padWithZero(pct.toFixed(2),5)}% complete`)
   }
   for (const f of cars) {
      const reader = await CarIndexedReader.fromFile(f)
      cids.push(await client.storeCar(reader, { name: f, onStoredChunk } ))
   }
   return cids
}


const cars = yargs(process.argv.slice(2)).argv._
console.log(cars)
storeCARs(cars)
