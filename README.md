# NFT.Storage CAR uploader

Uploads [CAR file](https://ipld.io/specs/transport/car/carv1/) to 
[NFT.Storage](https://nft.storage) service. Requires [Node.js](https://nodejs.org/en/download/).

## Usage

Get NFT.Storage auth token from Account->API Tokens and put it into *NFTSTORAGE_TOKEN* environment variable:

```
set NFTSTORAGE_TOKEN=xxxxxxxx
npm install
node upload-car.js <Path to CAR file>
```
