import yargs from 'yargs'
import { makeStorageClient } from './lib.js'

async function storeNFT(metadataFileName, nftFileName) {
  const client = makeStorageClient()

  const metadata = JSON.parse(fs.readFileSync(metadataFileName))
  const content = fs.readFileSync(nftFileName)
  const file = new File([content], nftFileName)
  const { cid } = await client.storeBlob(file)
  metadata.image = new URL(`ipfs://${cid}`)
  const nftCid = await client.store(metadata)
  console.log(`Stored NFT with CID: ${nftCid}`)
}

const args = yargs(process.argv.slice(2)).argv._
