import { NFTStorage } from 'nft.storage'

export function getAccessToken() {
   const ENV_TOKEN = 'NFTSTORAGE_TOKEN'
   const token = process.env[ENV_TOKEN]
   if (!token) {
                  console.error("Environment variable "+ENV_TOKEN+" is not set")
                  process.exit(2)
   }
   return token
}

export function makeStorageClient () {
   return new NFTStorage( { token: getAccessToken() } )
}
